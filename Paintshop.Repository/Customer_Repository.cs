﻿// <copyright file="Customer_Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Paintshop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Paintshop.Data;

    /// <summary>
    /// Repository to manage customer table.
    /// </summary>
    public class Customer_Repository : IRepository<Customer>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Customer_Repository"/> class.
        /// </summary>
        public Customer_Repository()
        {
            this.Db = new Paintshop_dbEntities1();
        }

        /// <summary>
        /// Gets or sets database instance.
        /// </summary>
        private Paintshop_dbEntities1 Db { get; set; }

        /// <summary>
        /// Delete customer.
        /// </summary>
        /// <param name="id">ID to instance.</param>
        /// <returns>Returne message.</returns>
        public string Delete(int id)
        {
            // Should check if 'id' is a valid id to a instance.
            using (Paintshop_dbEntities1 db = new Paintshop_dbEntities1())
            {
                var delete = db.Customers.Find(id);
                try
                {
                    db.Customers.Remove(delete);
                    db.SaveChanges();
                    return "Instance removed successfully!";
                }
                catch (Exception e)
                {
                    return "Error: " + e.Message;
                }
            }
        }

        /// <summary>
        /// Querry all instance.
        /// </summary>
        /// <returns>IEnumerabley(List).</returns>
        public IEnumerable<Customer> GetAll()
        {
            List<Customer> result = this.Db.Customers.ToList();
            return result;
        }

        /// <summary>
        /// Insert Customer.
        /// </summary>
        /// <param name="parameters">Given parameters.</param>
        /// <returns>Return message.</returns>
        public string Insert(List<string> parameters)
        {
            Customer newCustomer = new Customer
            {
                Id = int.Parse(parameters.ElementAt(0)),
                Name = parameters.ElementAt(1),
                Date_of_birth = DateTime.Parse(parameters.ElementAt(2)),
                Place_of_birth = parameters.ElementAt(3),
                Mothers_name = parameters.ElementAt(4),
                Regular_customer = int.Parse(parameters.ElementAt(5)),
                Purchases = null,
            };
            try
            {
                this.Db.Customers.Add(newCustomer);
                this.Db.SaveChanges();
                return "Instance addedd successfully!";
            }
            catch (Exception e)
            {
                return "Error: " + e.Message;
            }
        }

        /// <summary>
        /// Update an instance.
        /// </summary>
        /// <param name="parameters">Given parameters.</param>
        /// <returns>Return message.</returns>
        public string Update(List<string> parameters)
        {
            Customer newCustomer = new Customer
            {
                Id = int.Parse(parameters.ElementAt(0)),
                Name = parameters.ElementAt(1),
                Date_of_birth = DateTime.Parse(parameters.ElementAt(2)),
                Place_of_birth = parameters.ElementAt(3),
                Mothers_name = parameters.ElementAt(4),
                Purchases = null,
            };
            newCustomer.Regular_customer = parameters.ElementAt(5) == "True" || parameters.ElementAt(5) == "1" ? 1 : 0;
            try
            {
                var update = this.Db.Customers.SingleOrDefault(x => x.Id == newCustomer.Id);
                if (update != null)
                {
                    update.Name = newCustomer.Name;
                    update.Mothers_name = newCustomer.Mothers_name;
                    update.Place_of_birth = newCustomer.Place_of_birth;
                    update.Regular_customer = newCustomer.Regular_customer;
                    this.Db.SaveChanges();
                }

                return "Instance updated successfully!";
            }
            catch (Exception e)
            {
                return "Error: " + e.Message;
            }
        }

        /// <summary>
        /// Non CRUD method: Get all customers born in 'Debrecen'.
        /// </summary>
        /// <returns>IEnumerabley(List).</returns>
        public IEnumerable<Customer> GetAllFromDebrecen()
        {
            List<Customer> result = this.Db.Customers.Where(x => x.Place_of_birth == "Debrecen").ToList();
            return result;
        }
    }
}
