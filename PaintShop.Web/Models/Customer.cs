﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PaintShop.Web.Models
{
    public class Customer
    {
        [Required]
        [Display(Name = "Vásárló ID")]
        public int Id { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 5)]
        [Display(Name = "Név")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Születési idő")]
        public DateTime Date_of_birth { get; set; }

        [Required]
        [Display(Name = "Születési hely")]
        [StringLength(50, MinimumLength = 3)]
        public string Place_of_birth { get; set; }

        [Required]
        [Display(Name = "Anyja neve")]
        [StringLength(50, MinimumLength = 5)]
        public string Mothers_name { get; set; }

        [Required]
        [Display(Name = "Rendszeres vásárló")]
        public bool Regular_customer { get; set; }

    }
}