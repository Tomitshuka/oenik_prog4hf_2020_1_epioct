﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PaintShop.Web.Models
{
    public class CustomerViewModel
    {
        public Customer EditedCustomer { get; set; }
        public List<Customer> ListOfCustomers { get; set; }

    }
}