﻿using AutoMapper;
using AutoMapper.Configuration.Conventions;
using Paintshop.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PaintShop.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Paintshop.Data.Customer, PaintShop.Web.Models.Customer>().
                    ForMember(dest => dest.Id, map => map.MapFrom(src => src.Id)).
                    ForMember(dest => dest.Name, map => map.MapFrom(src => src.Name)).
                    ForMember(dest => dest.Date_of_birth, map => map.MapFrom(src => src.Date_of_birth)).
                    ForMember(dest => dest.Place_of_birth, map => map.MapFrom(src => src.Place_of_birth)).
                    ForMember(dest => dest.Mothers_name, map => map.MapFrom(src => src.Mothers_name)).
                    ForMember(dest => dest.Regular_customer, map => map.MapFrom(src => src.Regular_customer));
            });
            return config.CreateMapper();
        }


    }
}