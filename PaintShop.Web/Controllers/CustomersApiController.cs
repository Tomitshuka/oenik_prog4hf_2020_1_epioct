﻿using AutoMapper;
using Paintshop.Data;
using Paintshop.Logic;
using Paintshop.Repository;
using PaintShop.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Paintshop.Web.Controllers
{
    public class CustomersApiController : ApiController
    {
        Logic.Logic logic;
        IMapper mapper;

        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        public CustomersApiController()
        {
            Paintshop_dbEntities1 ctx = new Paintshop_dbEntities1();
            Customer_Repository customerRepo = new Customer_Repository();
            logic = new Logic.Logic();
            mapper = MapperFactory.CreateMapper();

        }

        // GET: api/CustomersApi/all
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<PaintShop.Web.Models.Customer> GetAll()
        {
            var customers = logic.GetAllFromCustomers().ToList();

            return mapper.Map<IList<Data.Customer>, List<PaintShop.Web.Models.Customer>>(customers);
        }

        // GET: api/CustomersApi/del/{id}
        [ActionName("del")]
        [HttpGet]
        public ApiResult DeleteCustomer(int id)
        {
            bool success = logic.DeleteFromCustomers(id) == "Instance removed successfully!" ? true : false;
            return new ApiResult() { OperationResult = success };
        }


        // POST: api/CustomersApi/add + car
        [ActionName("add")]
        [HttpPost]
        public ApiResult AddCustomer(Data.Customer customer)
        {
            List<string> paramteres = new List<string>();
            paramteres.Add(customer.Id.ToString());
            paramteres.Add(customer.Name);
            paramteres.Add(customer.Date_of_birth.ToString());
            paramteres.Add(customer.Place_of_birth);
            paramteres.Add(customer.Mothers_name);
            paramteres.Add(customer.Regular_customer.ToString());
            
            logic.CreateToCustomers(paramteres);

            return new ApiResult() { OperationResult = true };
        }



        // POST: api/CustomersApi/del/{id}
        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModCustomer(Data.Customer customer)
        {
            List<string> paramteres = new List<string>();
            paramteres.Add(customer.Id.ToString());
            paramteres.Add(customer.Name);
            paramteres.Add(customer.Date_of_birth.ToString());
            paramteres.Add(customer.Place_of_birth);
            paramteres.Add(customer.Mothers_name);
            paramteres.Add(customer.Regular_customer.ToString());

            bool success = logic.UpdateInCustomer(paramteres) == "Instance updated successfully!" ? true : false;

            return new ApiResult() { OperationResult = success };
        }

    }
}
