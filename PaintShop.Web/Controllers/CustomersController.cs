﻿using AutoMapper;
using Paintshop.Data;
using Paintshop.Logic;
using Paintshop.Repository;
using PaintShop.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Paintshop.Web.Controllers
{
    public class CustomersController : Controller
    {
        // Constructor
        Logic.Logic logic;
        IMapper mapper;
        CustomerViewModel model;

        public CustomersController()
        {
            Paintshop_dbEntities1 ctx = new Paintshop_dbEntities1();
            Customer_Repository customerRepo = new Customer_Repository();
            logic = new Logic.Logic();
            mapper = MapperFactory.CreateMapper();
            model = new CustomerViewModel();
            model.EditedCustomer = new PaintShop.Web.Models.Customer();

            var customers = logic.GetAllFromCustomers().ToList();
            model.ListOfCustomers = mapper.Map<IList<Data.Customer>, List<PaintShop.Web.Models.Customer>>(customers);
        }

        private PaintShop.Web.Models.Customer GetCustomerModel(int id)
        {
            var customer = logic.GetCustomerByID(id);
            return mapper.Map<Data.Customer, PaintShop.Web.Models.Customer>(customer);
        }

        // GET: Customers
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("CustomersIndex", model);
        }

        // GET: Customers/Details/5
        public ActionResult Details(int id)
        {
            return View("CustomersDetails", GetCustomerModel(id));
        }

        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete FAIL";
            if (logic.DeleteFromCustomers(id) == "Instance removed successfully!")
            {
                TempData["editResult"] = "Delete OK";
            }
            return RedirectToAction("Index");
        }

        // GET: Customers/Edit/5
        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            model.EditedCustomer = GetCustomerModel(id);
            return View("CustomersIndex", model);
        }

        // POST: Customers/Edit/5
        [HttpPost]
        public ActionResult Edit(PaintShop.Web.Models.Customer customer, string editAction)
        {
            if (ModelState.IsValid && customer != null)
            {
                List<string> paramteres = new List<string>();
                paramteres.Add(customer.Id.ToString());
                paramteres.Add(customer.Name);
                paramteres.Add(customer.Date_of_birth.ToString());
                paramteres.Add(customer.Place_of_birth);
                paramteres.Add(customer.Mothers_name);
                paramteres.Add(customer.Regular_customer.ToString());
                TempData["editResult"] = "Edit OK";

                if (editAction == "AddNew")
                {
                    logic.CreateToCustomers(paramteres);
                }
                else
                {
                    if (logic.UpdateInCustomer(paramteres) != "Instance updated successfully!")
                    {
                        TempData["editResult"] = "Edit FAIL";
                    }
                }
                return RedirectToAction("Index");
            }
            else
            {
                ViewData["editAction"] = "Edit";
                model.EditedCustomer = customer;
                return View("CustomersIndex", model);
            }
        }
    }

}
