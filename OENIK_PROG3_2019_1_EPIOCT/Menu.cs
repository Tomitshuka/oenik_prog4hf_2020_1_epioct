﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_PROG3_2019_1_EPIOCT
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using Paintshop.Data;
    using Paintshop.Logic;

    /// <summary>
    /// Menu class.
    /// </summary>
    public class Menu
    {
        /// <summary>
        /// Keeps alive the while cycle.
        /// </summary>
        private bool running = true;

        /// <summary>
        /// A cycle while the user not insert '10'.
        /// </summary>
        public void Run()
        {
            this.Help();
            while (this.running)
            {
                string inputFromConsole = Console.ReadLine();
                int option = 0;
                try
                {
                    option = int.Parse(inputFromConsole);
                }
                catch (System.FormatException)
                {
                    // Console.WriteLine("Please give a number between 1-10!");
                }
                finally
                {
                    Logic logic = new Logic();
                    switch (option)
                    {
                        case 1:
                            {
                                Console.WriteLine("You've pressed 1!");
                                List<Customer> result = new List<Customer>(logic.GetAllFromCustomers());
                                Console.WriteLine("ID\tName \t\tDate of birth \t\tPlace of birth \tMothers name \t Regular customer");
                                foreach (var item in result)
                                {
                                    Console.WriteLine(item.Id + "\t" + item.Name + "\t" + item.Date_of_birth + "\t" + item.Place_of_birth + "\t" + item.Mothers_name + "\t"
                                        + item.Regular_customer);
                                }

                                Console.WriteLine("\nPlease give a number between 1-10! ('9' to Help, '10' to exit)");
                                break;
                            }

                        case 2:
                            {
                                Console.WriteLine("You've pressed 2!");
                                Console.WriteLine("Choose a table to add instance ('Customres', '')!");
                                string tableGiven = Console.ReadLine();
                                List<string> parameters = new List<string>();
                                Console.WriteLine("Add ID");
                                parameters.Add(Console.ReadLine());
                                Console.WriteLine("Add name");
                                parameters.Add(Console.ReadLine());
                                Console.WriteLine("Add date of birth (YYYY-MM-DD)");
                                parameters.Add(Console.ReadLine());
                                Console.WriteLine("Add place of birth");
                                parameters.Add(Console.ReadLine());
                                Console.WriteLine("Add mothers name");
                                parameters.Add(Console.ReadLine());
                                Console.WriteLine("Regular customer (0 - NO, 1 - YES)");
                                parameters.Add(Console.ReadLine());
                                string result = logic.CreateToCustomers(parameters);
                                Console.WriteLine(result);
                                Console.WriteLine("\nPlease give a number between 1-10! ('9' to Help, '10' to exit)");
                                break;
                            }

                        case 3:
                            {
                                Console.WriteLine("You've pressed 3!");
                                Console.WriteLine("Choose a table to delete from ('Customres', '')!");
                                string tableGiven = Console.ReadLine();
                                Console.WriteLine("Give the ID of the removable instance!");
                                string idGiven = Console.ReadLine();
                                string result = logic.DeleteFromCustomers(int.Parse(idGiven));
                                Console.WriteLine(result);
                                Console.WriteLine("\nPlease give a number between 1-10! ('9' to Help, '10' to exit)");
                                break;
                            }

                        case 4:
                            {
                                Console.WriteLine("You've pressed 4!");
                                Console.WriteLine("Choose a table to update in ('Customres', '')!");
                                string tableGiven = Console.ReadLine();
                                Console.WriteLine("Give the ID of the updatable instance!");
                                List<string> parameters = new List<string>();
                                parameters.Add(Console.ReadLine());
                                Console.WriteLine("Add name");
                                parameters.Add(Console.ReadLine());
                                Console.WriteLine("Add date of birth (YYYY-MM-DD)");
                                parameters.Add(Console.ReadLine());
                                Console.WriteLine("Add place of birth");
                                parameters.Add(Console.ReadLine());
                                Console.WriteLine("Add mothers name");
                                parameters.Add(Console.ReadLine());
                                Console.WriteLine("Regular customer (0 - NO, 1 - YES)");
                                parameters.Add(Console.ReadLine());
                                string result = logic.UpdateInCustomer(parameters);
                                Console.WriteLine(result);
                                Console.WriteLine("\nPlease give a number between 1-10! ('9' to Help, '10' to exit)");
                                break;
                            }

                        case 5:
                            {
                                Console.WriteLine("You've pressed 5!");
                                List<Customer> result = new List<Customer>(logic.GetAllFromDebrecen());

                                foreach (var item in result)
                                {
                                    Console.WriteLine(item.Id + "\t" + item.Name + "\t" + item.Date_of_birth);
                                }

                                Console.WriteLine("\nPlease give a number between 1-10! ('9' to Help, '10' to exit)");
                                break;
                            }

                        case 6:
                            {
                                Console.WriteLine("You've pressed 6!");
                                List<Shop> result = new List<Shop>(logic.GetAllInBudapest());

                                foreach (var item in result)
                                {
                                    Console.WriteLine(item.Id + "\t" + item.Address + "\t" + item.Settlement + "\t" + item.Zipcode
                                        + "\t" + item.Shopkeeper + "\t" + item.Opening_hours);
                                }

                                Console.WriteLine("\nPlease give a number between 1-10! ('9' to Help, '10' to exit)");
                                break;
                            }

                        case 7:
                            {
                                Console.WriteLine("You've pressed 7!");
                                List<Item> result = new List<Item>(logic.GetAllPerishable());

                                foreach (var item in result)
                                {
                                    Console.WriteLine(item.Id + "\t" + item.Name + "\t" + item.Price + " Ft \t" + item.Perishable_Date);
                                }

                                Console.WriteLine("\nPlease give a number between 1-10! ('9' to Help, '10' to exit)");
                                break;
                            }

                        case 8:
                            {
                                Console.WriteLine("Java web");
                                Console.WriteLine("You've pressed 8!");
                                Console.WriteLine("\nPlease give a number between 1-10! ('9' to Help, '10' to exit)");
                                break;
                            }

                        case 9:
                            {
                                this.Help();
                                break;
                            }

                        case 10:
                            {
                                this.running = false;
                                Console.WriteLine("Goodbye");
                                Thread.Sleep(500);
                                break;
                            }

                        default:
                            {
                                Console.WriteLine("Please give a number between 1-10! ('9' to Help, '10' to exit)");
                                break;
                            }
                    }
                }
            }
        }

        /// <summary>
        /// Help method.
        /// </summary>
        public void Help()
        {
            Console.Clear();
            Console.WriteLine("Choose an option");
            Console.WriteLine("1.: List table content");
            Console.WriteLine("2.: Add new instance to table.");
            Console.WriteLine("3.: Delete instance from table. (by ID)");
            Console.WriteLine("4.: Update instnce in a table (by ID)");
            Console.WriteLine("5.: List all customers born in 'Debrecen' (NON_CRUD)");
            Console.WriteLine("6.: List all shops in Budapest (Zip code starts with '1')(NON_CRUD)");
            Console.WriteLine("7.: List all items which will be run down (NON_CRUD)");
            Console.WriteLine("8.: Java web");
            Console.WriteLine("9.: Help");
            Console.WriteLine("10.: Exit");
        }
    }
}
