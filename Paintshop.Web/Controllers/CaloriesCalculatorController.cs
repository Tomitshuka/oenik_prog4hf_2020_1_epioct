﻿using CalorieCalculator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CalorieCalculator.Controllers
{
    public class CaloriesCalculatorController : Controller
    {
        // GET: CaloriesCalculator
        public ActionResult Index()
        {
            return View();
        }

        // GET: Calculator
        [HttpGet]
        public ActionResult Calculator()
        {
            return View();
        }

        // POST: Calculator
        [HttpPost]
        public ActionResult Calculator(Training exercise)
        {
            // Some checks
            // Notify that this is NOT a full validation (anyway this could be handled in Javascript)

            if (exercise.Name == null)
            {
                TempData["myWarning"] = "Hiányzó adat: 'Sportoló neve'";
                return this.RedirectToAction(nameof(Calculator));
            }

            if (exercise.Practice == null)
            {
                TempData["myWarning"] = "Hiányzó adat: 'Gyakorlat'";
                return this.RedirectToAction(nameof(Calculator));
            }

            if (exercise.Weight <= 0)
            {
                TempData["myWarning"] = "A 'Sportoló tömege' adatnak nagyobbnak kell lennie 0-nál";
                return this.RedirectToAction(nameof(Calculator));
            }

            if (exercise.Minutes <= 0)
            {
                TempData["myWarning"] = "A 'Gyakorlat hossza (perc)' adatnak nagyobbnak kell lennie 0-nál";
                return this.RedirectToAction(nameof(Calculator));
            }

            // Calculate result
            Training result = new Training()
            {
                Name = exercise.Name,
                Weight = exercise.Weight,
                Practice = exercise.Practice,
                Minutes = exercise.Minutes
            };

            List<Exercise> exercises = new List<Exercise>();

            exercises.Add(new Exercise("Running", 1000));
            exercises.Add(new Exercise("Yoga", 400));
            exercises.Add(new Exercise("Pilates", 472));
            exercises.Add(new Exercise("Hiking", 700));
            exercises.Add(new Exercise("Swimming", 1000));
            exercises.Add(new Exercise("Bicycle", 600));

            // Sets the input Training 'BurnedCalories' property
            result.Calculate(result, exercises);

            return View("Result", result);
        }
    }
}