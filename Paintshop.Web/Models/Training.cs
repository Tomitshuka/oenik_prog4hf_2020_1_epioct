﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CalorieCalculator.Models
{
    public class Training
    {
        [Display(Name = "Sportoló neve")]
        public string Name { get; set; }

        [Display(Name = "Sportoló tömege")]
        public double Weight { get; set; }

        [Display(Name = "Gyakorlat")]
        public string Practice { get; set; }

        [Display(Name = "Gyakorlat hossza (perc)")]
        public double Minutes { get; set; }

        [Display(Name = "Elégett kalória")]
        public double BurnedCalories { get; set; }


        public void Calculate(Training training, List<Exercise> exercises)
        {
            Exercise selected_exercise = null;

            foreach (var exercise in exercises)
            {
                if(exercise.ExerciseName.Equals(training.Practice))
                {
                    selected_exercise = exercise;
                }
            }

            if(selected_exercise == null)
            {
                return; // Jump out from function
            }

            // divide the default calories (60 minutes * 100kg) by 6000 (60 * 100) and multiply with the input weight and minutes
            double burned_calories = (selected_exercise.DefaultBurnCalories / 6000) * training.Weight * training.Minutes;

            training.BurnedCalories = burned_calories;
        }

    }
}