﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalorieCalculator.Models
{
    public class Exercise
    {
        public string ExerciseName { get; set; }

        public double DefaultBurnCalories { get; set; } // 100kg person, 1 hour of exercise

        public Exercise(string name, double calories)
        {
            ExerciseName = name;
            DefaultBurnCalories = calories;
        }

    }
}