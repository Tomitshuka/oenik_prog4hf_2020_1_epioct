﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Paintshop.ConsoleClient
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date_of_birth { get; set; }
        public string Place_of_birth { get; set; }
        public string Mothers_name { get; set; }
        public bool Regular_customer { get; set; }

        public override string ToString()
        {
            return Id.ToString() + "\t" + Name + "\t" + Date_of_birth.ToString() + "\t" + Place_of_birth;
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://localhost:64465/api/CustomersApi";

            Console.WriteLine("WAITING FOR WEB....");

            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "/all").Result;
                var list = JsonConvert.DeserializeObject<List<Customer>>(json);

                foreach (var item in list) Console.WriteLine(item);
                Console.ReadLine();

                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Customer.Name), "Borbély István");
                postData.Add(nameof(Customer.Date_of_birth), "1997-08-12");
                postData.Add(nameof(Customer.Place_of_birth), "Nagykanizsa");
                postData.Add(nameof(Customer.Mothers_name), "Kiss Erzsébet");
                postData.Add(nameof(Customer.Regular_customer), "1");

                response = client.PostAsync(url + "/add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "/all").Result;
                Console.WriteLine("ADD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.WriteLine("\nPress enter to continue the program");
                Console.ReadLine();

                // Change 'Borbély István' regular customer to false

                int customerId = JsonConvert.DeserializeObject<List<Customer>>(json).Single(x => x.Name == "Borbély István").Id;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Customer.Id), customerId.ToString());
                postData.Add(nameof(Customer.Name), "Borbély István");
                postData.Add(nameof(Customer.Date_of_birth), "1997-08-12");
                postData.Add(nameof(Customer.Place_of_birth), "Nagykanizsa");
                postData.Add(nameof(Customer.Mothers_name), "Kiss Erzsébet");
                postData.Add(nameof(Customer.Regular_customer), "0");

                response = client.PostAsync(url + "/mod",
                new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + client.GetStringAsync(url + "/all").Result);
                Console.WriteLine("\nPress enter to continue the program");
                Console.ReadLine();

                Console.WriteLine("DEL: " + client.GetStringAsync(url + "/del/" + customerId).Result);
                Console.WriteLine("ALL: " + client.GetStringAsync(url + "/all").Result);
                Console.WriteLine("\nPress any key to continue...");
                Console.ReadKey();
            }
        }
    }
}
