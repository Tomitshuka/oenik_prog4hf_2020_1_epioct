﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Paintshop.Wpf
{
    // To test the Wpf project: Set startup projects: Paintshop.Web + Paintshop.Wpf
    class MainLogic
    {
        string url = "http://localhost:64465/api/CustomersApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "CustomerResult");
        }

        public List<CustomerVM> ApiGetCustomers()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<CustomerVM>>(json);
            return list;
        }

        public void ApiDelCustomer(CustomerVM customer)
        {
            bool success = false;
            if (customer != null)
            {
                string json = client.GetStringAsync(url + "del/" + customer.Id.ToString()).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }
        bool ApiEditCustomer(CustomerVM customer, bool isEditing)
        {
            if (customer == null) return false;
            string myUrl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add(nameof(CustomerVM.Id), customer.Id.ToString());
            }

            postData.Add(nameof(CustomerVM.Name), customer.Name);
            postData.Add(nameof(CustomerVM.Date_of_birth), customer.Date_of_birth.ToString());
            postData.Add(nameof(CustomerVM.Place_of_birth), customer.Place_of_birth);
            postData.Add(nameof(CustomerVM.Mothers_name), customer.Mothers_name);
            if(customer.Regular_customer)
                postData.Add(nameof(CustomerVM.Regular_customer), "1");
            else
                postData.Add(nameof(CustomerVM.Regular_customer), "0");

            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);

            return (bool)obj["OperationResult"];
        }

        public void EditCustomer(CustomerVM customer, Func<CustomerVM, bool> editor)
        {
            CustomerVM clone = new CustomerVM();
            if (customer != null) clone.CopyFrom(customer);
            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (customer != null) success = ApiEditCustomer(clone, true);
                else success = ApiEditCustomer(clone, false);
            }
            SendMessage(success == true);
        }

    }
}
