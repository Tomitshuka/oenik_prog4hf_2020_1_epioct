﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows.Input;

namespace Paintshop.Wpf
{
	// To test the Wpf project: Set startup projects: Paintshop.Web + Paintshop.Wpf
	class MainVM : ViewModelBase
    {
		private MainLogic logic;
		private ObservableCollection<CustomerVM> allCustomers;
		private CustomerVM selectedCustomer;

		public CustomerVM SelectedCustomer
		{
			get { return selectedCustomer; }
			set { Set(ref selectedCustomer, value); }
		}
		public ObservableCollection<CustomerVM> AllCustomers
		{
			get { return allCustomers; }
			set { Set(ref allCustomers, value); }
		}

		public ICommand AddCmd { get; private set; }
		public ICommand DelCmd { get; private set; }
		public ICommand ModCmd { get; private set; }
		public ICommand LoadCmd { get; private set; }

		public Func<CustomerVM, bool> EditorFunc { get; set; }

		public MainVM()
		{
			logic = new MainLogic();

			LoadCmd = new RelayCommand(() => AllCustomers = new ObservableCollection<CustomerVM>(logic.ApiGetCustomers()));
			DelCmd = new RelayCommand(() => logic.ApiDelCustomer(selectedCustomer));
			AddCmd = new RelayCommand(() => logic.EditCustomer(null, EditorFunc));
			ModCmd = new RelayCommand(() => logic.EditCustomer(selectedCustomer, EditorFunc));
		}


	}
}
