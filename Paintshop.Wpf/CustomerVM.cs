﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paintshop.Wpf
{
	// To test the Wpf project: Set startup projects: Paintshop.Web + Paintshop.Wpf
	class CustomerVM : ObservableObject
	{
		private int id;
		private string name;
		private DateTime date_of_birth;
		private string place_of_birth;
		private string mothers_name;
		private bool regular_customer;

		public int Id
		{
			get { return id; }
			set { Set(ref id, value); }
		}

		public string Name
		{
			get { return name; }
			set { Set(ref name, value); }
		}

		public DateTime Date_of_birth
		{
			get { return date_of_birth; }
			set { Set(ref date_of_birth, value); }
		}

		public string Place_of_birth
		{
			get { return place_of_birth; }
			set { Set(ref place_of_birth, value); }
		}

		public string Mothers_name
		{
			get { return mothers_name; }
			set { Set(ref mothers_name, value); }
		}

		public bool Regular_customer
		{
			get { return regular_customer; }
			set { Set(ref regular_customer, value); }
		}

		public void CopyFrom(CustomerVM other)
		{
			if (other == null)
				return;
			this.Id = other.Id;
			this.Name = other.Name;
			this.Date_of_birth = other.Date_of_birth;
			this.Place_of_birth = other.Place_of_birth;
			this.Mothers_name = other.Mothers_name;
			this.Regular_customer = other.Regular_customer;
		}

	}
}
